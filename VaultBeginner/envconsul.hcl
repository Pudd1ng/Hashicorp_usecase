vault {
  address = "http://127.0.0.1:8200"
  grace = "15s"
  renew_token = false
  token = "9f1f9fbc-97db-6ceb-16d8-8da4fd85c7da"
  retry {
    # ...
  }
  ssl {
    enabled = false
  }
}
wait {
  min = "5s"
  max = "10s"
}
# README

## The following repository will auto install vault without the use of TLS as well as envconsul

### 1. Once vault is up and running with vagrant up you can initialise vault using
vault init

This will give keys based on your config.hcl(default 5 thresh 3) and the root authentication token.
To authenticate as root
vault auth (roottoken)

### 2. To write a standard secret to vault
vault write secret/conner value=bell 

to read it 
vault read secret/conner

### 3. To mount the mysql backend and generate and endpoint to create creds

We have to use the legacy database plugin with older versions of mysql

vault mount database

vault write database/config/mysql \
    plugin_name=mysql-legacy-database-plugin \
    connection_url="root:root@tcp(127.0.0.1:3306)/" \
    allowed_roles="readonly"

This will now be used as vaults connection to the mysql instance.

Now we have to create a role for us creation.

vault write database/roles/readonly \
    db_name=mysql \
    creation_statements="CREATE USER '{{name}}'@'%' IDENTIFIED BY '{{password}}';GRANT SELECT ON *.* TO '{{name}}'@'%';" \
    default_ttl="1h" \
    max_ttl="24h"

To get our credentials we can use the vault api 

ee931b42-82a8-dec6-ec51-a2cf1bf1a020 - root token example

curl \
    -H "X-Vault-Token: ee931b42-82a8-dec6-ec51-a2cf1bf1a020" \
    -X GET \
    http://127.0.0.1:8200/v1/database/creds/readonly

### 4. To populate environment variables based off of secrets

vault write secret/conner username=foo password=bar

envconsul \
    -config="/opt/envconfig.hcl" \
    -secret="secret/conner" \
    env

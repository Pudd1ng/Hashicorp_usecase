#Consul Bootstrap
 #!/bin/bash
sudo yum update -y
sudo yum install -y httpd24 php56 mysql55-server php56-mysqlnd 
sudo service httpd start
sudo chkconfig httpd on
sudo groupadd www
sudo usermod -a -G www ec2-user
sudo chown -R root:www /var/www
sudo chmod 2775 /var/www
sudo find /var/www -type d -exec chmod 2775 {} +
sudo find /var/www -type f -exec chmod 0664 {} +
sudo touch /var/www/html/index.html
sudo wget https://releases.hashicorp.com/consul/0.9.2/consul_0.9.2_linux_amd64.zip
sudo unzip consul_0.9.2_linux_amd64.zip
sudo chmod +x consul
sudo cp consul /usr/bin/consul
sudo mkdir /opt/consul
sudo mkdir /usr/consul.d
sudo cp /tmp/* /usr/consul.d
sudo chown -R ec2-user:root /opt/consul
sudo chown -R ec2-user:root /usr/consul.d
sudo chmod 2555 /usr/consul.d
sudo echo "Finished Web, Starting consul.........................."
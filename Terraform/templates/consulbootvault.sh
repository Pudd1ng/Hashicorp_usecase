#Installing Vault
sudo yum update -y
sudo wget https://releases.hashicorp.com/consul/0.9.2/consul_0.9.2_linux_amd64.zip
sudo unzip consul_0.9.2_linux_amd64.zip
sudo chmod +x consul
sudo cp consul /usr/bin/consul
sudo mkdir /opt/consul
sudo mkdir /usr/consul.d
sudo chown -R ec2-user:root /opt/consul
sudo chown -R ec2-user:root /usr/consul.d
sudo chmod 2555 /usr/consul.d
sudo cp /tmp/vault /etc/init.d/vault
sudo chmod +x /etc/init.d/vault
sudo chown 777 /etc/init.d/vault
sudo wget https://releases.hashicorp.com/vault/0.8.3/vault_0.8.3_linux_amd64.zip
sudo unzip vault_0.8.3_linux_amd64.zip
sudo cp vault /usr/bin
sudo echo "Setting vault addr"
sudo echo VAULT_ADDR="http://127.0.0.1:8200" >> /etc/profile
sudo cp /tmp/config.hcl /opt/config.hcl

#Consul bootstrap
#!/bin/bash
sudo yum update -y
sudo yum install -y mysql-server 
sudo service mysqld start
sudo chkconfig mysqld on
sudo groupadd db
sudo usermod -a -G db ec2-user
sudo chown -R root:db /var/lib/mysql
sudo chmod 2775 /var/lib/mysql
sudo wget https://releases.hashicorp.com/consul/0.9.2/consul_0.9.2_linux_amd64.zip
sudo unzip consul_0.9.2_linux_amd64.zip
sudo chmod +x consul
sudo cp consul /usr/bin/consul
sudo mkdir /opt/consul
sudo mkdir /usr/consul.d
sudo cp /tmp/mysql.json /usr/consul.d/mysql.json
sudo chown -R ec2-user:root /opt/consul
sudo chown -R ec2-user:root /usr/consul.d
sudo chmod 2555 /usr/consul.d
sudo chmod 2777 /tmp/checkmysql.sh
sudo chmod +x /tmp/checkmysql.sh
sudo echo "Finished db, Starting consul.........................."

#VPC Variables
variable "vpcname" {}
variable "vpccidr" {}
variable "publicsubnetarray" { type = "list"}
variable "azs" { type = "list"}
#Load Balancer Variables
variable "lbname" {}
variable "instanceport" {}
variable "instanceprotocol" {}
variable "lbport" {}
variable "lbprotocol" {}
#instance variables
variable "instance_type" {}
variable "instance_name" {}
variable "instance_name_2" {}
variable "instance_name_3" {}
variable "ami_id" {}
variable "number_of_instances" {}
variable "user_data" {}
variable "keyname" {}
variable "user_data_2" {}
variable "httpddest" {}
variable "httpdjson" {}
variable "mysqldest" {}
variable "mysqljson" {}
variable "ec2user" {}
variable "private_key_path" {}
variable "consulbootweb" {}
variable "consulbootdb" {}
variable "mysqldch" {}
variable "mysqldchdest" {}
variable "consulbootwebdes" {}
variable "consulbootdbdes" {}
variable "number_of_instances_vault" {}
variable "vaultinitd" {}
variable "vaultinitdest" {}
variable "confighcl" {}
variable "confighcldest" {}
variable "consulbootvault" {}
variable "consulbootvaultdes" {}
#variable "publickey" {}
#keys for community mods else store in ENV
#variable "aws_access_key" {}
#variable "aws_secret_key" {}
#variable "aws_region" {}

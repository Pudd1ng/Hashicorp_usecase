provider "aws" {}

module "vpc" {
  source             = "github.com/terraform-community-modules/tf_aws_vpc"
  name               = "${var.vpcname}"
  cidr               = "${var.vpccidr}"
  public_subnets     = "${var.publicsubnetarray}"
  azs                = "${var.azs}"
  map_public_ip_on_launch = true
  enable_dns_support      = true
  enable_dns_hostnames    = true
  tags {
    "Terraform"      = "true"
    "Environment"    = "usecase"
  }
}

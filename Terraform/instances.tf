resource "aws_instance" "db_instance" {
  ami                    = "${var.ami_id}"
  iam_instance_profile   = "${aws_iam_instance_profile.consul-join.name}"
  count                  = "${var.number_of_instances}"
  subnet_id              = "${module.vpc.public_subnets[0]}"
  instance_type          = "${var.instance_type}"
  #user_data              = "${file(var.user_data_2)}"
  key_name               = "${var.keyname}"
  vpc_security_group_ids = ["${aws_security_group.allow_all.*.id}"]
  tags {
    Name   = "${var.instance_name_2}-${count.index}"
    Consul = "autojoin"
  }

  connection {
    type        = "ssh"
    user        = "${var.ec2user}"
    private_key = "${file("${var.private_key_path}")}"
  }

  provisioner "file" {
    source      = "${var.mysqljson}"
    destination = "${var.mysqldest}"   
  }

  provisioner "file" {
    source      = "${var.mysqldch}"
    destination = "${var.mysqldchdest}"
  }

  provisioner "file" {
    source      = "${var.consulbootdb}"
    destination = "${var.consulbootdbdes}"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/consulbootdb.sh",
      "/tmp/consulbootdb.sh > /tmp/bootup.log",
    ]
  }

  provisioner "remote-exec" {
    inline = [
    "sudo nohup consul agent -server -bootstrap-expect 3 -data-dir '/opt/consul' -retry-join 'provider=aws tag_key=Consul tag_value=autojoin' -config-dir /usr/consul.d -enable-script-checks true > /tmp/consul.log &",
    "sleep 10s",
    "ps -aux | grep consul"
    ]
  }
}

resource "aws_instance" "web_instance" {
  ami                    = "${var.ami_id}"
  iam_instance_profile   = "${aws_iam_instance_profile.consul-join.name}"
  count                  = "${var.number_of_instances}"
  subnet_id              = "${module.vpc.public_subnets[0]}"
  instance_type          = "${var.instance_type}"
  key_name               = "${var.keyname}"
  vpc_security_group_ids = ["${aws_security_group.allow_all.*.id}"]
  tags {
    Name   = "${var.instance_name}-${count.index}"
    Consul = "autojoin"
  }
  depends_on         = ["aws_instance.db_instance"]
  connection {
    type        = "ssh"
    user        = "${var.ec2user}"
    private_key = "${file("${var.private_key_path}")}"
  }

  provisioner "file" {
    source      = "${var.httpdjson}"
    destination = "${var.httpddest}"
  }
  provisioner "file" {
    source      = "${var.consulbootweb}"
    destination = "${var.consulbootwebdes}"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/consulbootweb.sh",
      "/tmp/consulbootweb.sh > /tmp/bootup.log",
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "sudo nohup consul agent -ui -client 0.0.0.0 -data-dir '/opt/consul' -retry-join 'provider=aws tag_key=Consul tag_value=autojoin' -config-dir /usr/consul.d -enable-script-checks true  > /tmp/consul.log  &",
      "sleep 10s",
      "ps -aux | grep consul"
    ]
  }
}

resource "aws_instance" "vault_instance" {
  ami                    = "${var.ami_id}"
  iam_instance_profile   = "${aws_iam_instance_profile.consul-join.name}"
  count                  = "${var.number_of_instances_vault}"
  subnet_id              = "${module.vpc.public_subnets[0]}"
  instance_type          = "${var.instance_type}"
  key_name               = "${var.keyname}"
  vpc_security_group_ids = ["${aws_security_group.allow_all.*.id}"]
  tags {
    Name   = "${var.instance_name_3}-${count.index}"
    Consul = "autojoin"
  }
  depends_on         = ["aws_instance.db_instance"]
  connection {
    type        = "ssh"
    user        = "${var.ec2user}"
    private_key = "${file("${var.private_key_path}")}"
  }

  provisioner "file" {
    source      = "${var.vaultinitd}"
    destination = "${var.vaultinitdest}" 
  }

  provisioner "file" {
    source      = "${var.confighcl}"
    destination = "${var.confighcldest}"
  }

    provisioner "file" {
    source      = "${var.consulbootvault}"
    destination = "${var.consulbootvaultdes}"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/consulbootvault.sh",
      "sudo /tmp/consulbootvault.sh > /tmp/bootup.log",
      "sudo /etc/init.d/vault start",
      "sudo nohup consul agent -client 0.0.0.0 -data-dir '/opt/consul' -retry-join 'provider=aws tag_key=Consul tag_value=autojoin' -config-dir /usr/consul.d -enable-script-checks true  > /tmp/consul.log  &",
      "sleep 10s",
      "ps -aux | grep consul"
    ]
  }
}

resource "aws_iam_role" "consul-join" {
  name               = "usecase-consul-join"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "consul-join" {
  name        = "usecase-consul-join"
  description = "Allows Consul nodes to describe instances for joining."
  policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "ec2:DescribeInstances",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "consul-join" {
  name       = "usecase-consul-join"
  roles      = ["${aws_iam_role.consul-join.name}"]
  policy_arn = "${aws_iam_policy.consul-join.arn}"
}

resource "aws_iam_instance_profile" "consul-join" {
  name  = "usecase-consul-join"
  roles = ["${aws_iam_role.consul-join.name}"]
}

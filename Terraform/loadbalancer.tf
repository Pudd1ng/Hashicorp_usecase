resource "aws_elb" "usecaselb" {
  name                = "${var.lbname}"
  #availability_zones  = ["${var.azs[0]}"]
  subnets             = ["${module.vpc.public_subnets[0]}"]
  instances           = ["${aws_instance.web_instance.*.id}"]
  security_groups     = ["${aws_security_group.allow_all.*.id}"]
  listener {
    instance_port     = "${var.instanceport}"
    instance_protocol = "${var.instanceprotocol}"
    lb_port           = "${var.lbport}"
    lb_protocol       = "${var.lbprotocol}"
  }
  tags {
    "Terraform"      = "true"
    "Environment"    = "usecase"
  }
  depends_on         = ["aws_instance.web_instance"] 
}
#If you specify subnets, you get a VPC ELB. 
#If you specify availability zones, you get an EC2-classic ELB.
#If you specify both, you get an error.

output "instance_id_web" {
  value = ["${aws_instance.web_instance.*.id}"]
}

output "instance_ip_web" {
  value = ["${aws_instance.web_instance.*.public_ip}"]
}
output "instance_id_db" {
  value = ["${aws_instance.db_instance.*.id}"]
}

output "instance_ip_db" {
  value = ["${aws_instance.db_instance.*.public_ip}"]
}

output "instance_id_vault" {
  value = ["${aws_instance.vault_instance.*.id}"]
}

output "instance_ip_vault" {
  value = ["${aws_instance.vault_instance.*.public_ip}"]
}
output "loadbalancer_id" {
  value = "${aws_elb.usecaselb.id}"
}

output "vpc_id" {
  value = "${module.vpc.vpc_id}"
}

output "subnet_id" {
  value = ["${module.vpc.public_subnets[0]}"]
}

output "igw_id" {
  value = "${module.vpc.igw_id}"
}
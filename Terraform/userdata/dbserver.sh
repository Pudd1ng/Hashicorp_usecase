#!/bin/bash
yum update -y
yum install -y mysql-server 
service mysqld start
chkconfig mysqld on
groupadd db
usermod -a -G db ec2-user
chown -R root:db /var/lib/mysql
chmod 2775 /var/lib/mysql

# README

## The Terraform repository will bootstrap Vault and Consul. The following README will discuss the commands needed to complete the HA install of Vault.

## Create a PGP key using keybase.io

1. Download keybase.io
2. Go to profile and add a PGP key

## Setup Vault to use PGP

vault init -key-shares=1 -key-threshold=1 -pgp-keys="keybase:Pudd1ng" -address=http://127.0.0.1:8200

**Note:** If you want to use more users you can use the below example

vault init -key-shares=3 -key-threshold=2 \
    -pgp-keys="keybase:jefferai,keybase:vishalnayak,keybase:sethvargo"

You need the exact number of users per keyshare as each key requres a unique PGP key.

**Example output**

Unseal Key 1: wcFMA6fTZkg7bJrkARAAbw5+HsyneXS56OfN6knTi7NCBWYRENyqvipjXK+lYxDApgATzOqYE8BrugAj5Zn2DSnHBCfmk33V76a90Trgls2+2RS1dCt+7TI5mxa5KlWNctMQZIP19WombRrDfI/gS4wuPvTlzTSbHfuNelcO3kmekudExqZK4KMnwkoMnoZme7F1u86/gxWq33kJHx2ugWP+dFbdrPfKB6aN0nHUttL9GNwTgmcN1O4y5/O3DOFeTM/2CDFOOE2FYT7LvmzbZwX31aZPREs+9430tH0cgS6z6x8lqZ4oVJajkeCvDtROLpSRGqvFDgpWeqgSzeXKYACKtVYwm4WwHN7B1hf2UUDwnZXHlfT6sGS2svMRW20IXmnDw7nLLAzeL9Bwm/NH7sp4Twf2tspRnCHpuk52f36cZ3tDK6mhK7dHBHmXGape6vPH2yO/sduWM1VmNIWJ4QEiy/XDM2Pur53rHKiQIXGExgnCogeDvpf+4SilCVtbL2h8rz7X25xODuqareK/RA2/bHg4saXHfBaxyBKw1QMzZ7hNxDMBRKbrt4HLqBKpJVwuy4/gOHndlENEr+q1QU/4bhHF4SfgyX+teJVSptzljDteFlI8KSwQkaibJh4JNpW3tL8WCFfZOhFpCBnqG88taMWN3Ludh8Cw2Nr/0DIgea1NELgAy4H2BV8r7v3S4AHkTcUZogdkQCsrzxfkbm4Pb+FjleAm4CPheWXgFeIurWD64FDmHfVrleGt245eeKW4lzkBgvBb/AfdFZ9DXcY97Sgo9lEOUa56A09IzxsnC6EFET+XcN86yXys0OXwMlnPuZJ7rOBK5CgDROih2m8+PZjMqgRAW/7iKTtMReEtFQA=
Initial Root Token: dceae3d8-44b7-2cc8-c306-6463c87e9c39

## Decrpyting the key to get the vault plain text key

You need the keybase cli which should be installed if you downloaded keybase.io

echo "wcFMA6fTZkg7bJrkARAAbw5+HsyneXS56OfN6knTi7NCBWYRENyqvipjXK+lYxDApgATzOqYE8BrugAj5Zn2DSnHBCfmk33V76a90Trgls2+2RS1dCt+7TI5mxa5KlWNctMQZIP19WombRrDfI/gS4wuPvTlzTSbHfuNelcO3kmekudExqZK4KMnwkoMnoZme7F1u86/gxWq33kJHx2ugWP+dFbdrPfKB6aN0nHUttL9GNwTgmcN1O4y5/O3DOFeTM/2CDFOOE2FYT7LvmzbZwX31aZPREs+9430tH0cgS6z6x8lqZ4oVJajkeCvDtROLpSRGqvFDgpWeqgSzeXKYACKtVYwm4WwHN7B1hf2UUDwnZXHlfT6sGS2svMRW20IXmnDw7nLLAzeL9Bwm/NH7sp4Twf2tspRnCHpuk52f36cZ3tDK6mhK7dHBHmXGape6vPH2yO/sduWM1VmNIWJ4QEiy/XDM2Pur53rHKiQIXGExgnCogeDvpf+4SilCVtbL2h8rz7X25xODuqareK/RA2/bHg4saXHfBaxyBKw1QMzZ7hNxDMBRKbrt4HLqBKpJVwuy4/gOHndlENEr+q1QU/4bhHF4SfgyX+teJVSptzljDteFlI8KSwQkaibJh4JNpW3tL8WCFfZOhFpCBnqG88taMWN3Ludh8Cw2Nr/0DIgea1NELgAy4H2BV8r7v3S4AHkTcUZogdkQCsrzxfkbm4Pb+FjleAm4CPheWXgFeIurWD64FDmHfVrleGt245eeKW4lzkBgvBb/AfdFZ9DXcY97Sgo9lEOUa56A09IzxsnC6EFET+XcN86yXys0OXwMlnPuZJ7rOBK5CgDROih2m8+PZjMqgRAW/7iKTtMReEtFQA=" | base64 -D | keybase pgp decrypt

Example decrypted output

53d17864c7c21f6b2f405cc3e6b18ca965b8cd839de3119747053b3de5770893

## Proving HA

Unseal both vaults and notice how one of the becomes standby and one of them beomes active.
When you seal the active vault the standby vault shoudl then become active.

